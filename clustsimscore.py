#!/usr/bin/env python

"""
clustsimscore.py: calculate gene cluster similarity from pfam results

Method uses pfam composition similarity as described in:
Cimermancic, P., Medema, M. H., Claesen, J., Kurita, K., Wieland Brown, L. C., Mavrommatis, K., Fischbach, M. a. (2014).
Insights into secondary metabolism from a global analysis of prokaryotic biosynthetic gene clusters
"""
__author__ = "Mohammad Alanjary"
__version__ = "1.0.5"

import os, sys, tempfile, argparse, ast, time, itertools
import multiprocessing as mp
import ctypes
import numpy as np

clustmat=None

def getjd_vect(i,def_param=clustmat):
        mat1 = clustmat[:-i]
        mat2 = clustmat[i:]
        
        J = ((mat1*mat2)>0).sum(axis=1).astype(float) / ((mat1+mat2)>0).sum(axis=1).astype(float)
        DDS = np.exp(-(abs(mat1-mat2).sum(axis=1).astype(float)/np.maximum(mat1,mat2).sum(axis=1).astype(float)))

        return 0.36*J+0.64*DDS

def parsefile2(fil,xlist=[]):
    global clustmat
    xlist = set(xlist)
    allkeys = set()
    alldics = []
    allnames = []
    for line in fil:
        x = line.strip().split("\t")
        #temp=newdict(ast.literal_eval(x[1]))
        #temp.removekeys(xlist)
        temp = dict((k,y) for k,y in ast.literal_eval(x[1]).items() if k not in xlist)
        allkeys |= set(temp.keys())-xlist
        if len(temp):
            alldics.append(temp)
            allnames.append(x[0])
        #clustmat.append([x[0],set(temp.keys()),temp])
    keycol = {}
    for i,x in enumerate(list(allkeys)):
        keycol[x] = i
    
    cols = len(keycol)
    recs = len(allnames)
    
    basearray = mp.Array(ctypes.c_int8,cols*recs)
    clustmat = np.ctypeslib.as_array(basearray.get_obj()).reshape(recs,cols)
    
    for i,x in enumerate(alldics):
        for key in x:
            clustmat[i,keycol[key]]=x[key]
    return np.array(allnames)

def runlist(finput,fout,cpu,score,xlist=[]):
    print "Building cluster matrix..."
    if type(finput)==file:
        clustnames = parsefile2(finput,xlist)
    elif os.path.isfile(finput):
        with open(finput,"r") as fil:
            clustnames = parsefile2(fil,xlist)
            #clustdict=parsefile(fil)
    else:
        print "Error no clusterlist specified"
        exit()
    
    global clustmat
    
    tf = tempfile.NamedTemporaryFile(dir="./",prefix='result_',suffix='.tsv',delete=False)
    totnum = len(clustmat)*(len(clustmat)-1)/2
    print "Starting all vs all comparison ("+str(totnum)+" combinations)"
    j=[]
    if cpu>1:
        pool = mp.Pool(cpu)
        for i in xrange(1,len(clustmat)):
            j.append(pool.apply_async(getjd_vect, args=(i,)))
    
        status=[x.ready() for x in j]
        donejobs=np.zeros(len(status))
        donetotal=0
        while True:
            for i,x in enumerate(status):
                if x and not donejobs[i]:
                    rslt=j[i].get()
                    n1 = clustnames[:-(i+1)]
                    n2 = clustnames[(i+1):]
                    for l,r in enumerate(rslt):
                        if r>=score:
                            tf.write(n1[l]+"\t"+n2[l]+"\t"+str(r)+"\n")
                    # tf.write("\n".join(["%s\t%s\t%s"%(x[0],x[1],float(x[2])) for x in np.vstack((clustnames[:-(i+1)],clustnames[(i+1):],result)).T if float(x[2])>=score])+"\n")
                    donetotal+=len(rslt)
                    donejobs[i]=1
            time.sleep(1)
            print 'Wrote %s / %s records (%03.2f %%)' % (donetotal,totnum,100*float(donetotal)/totnum)
            status=[x.ready() for x in j]
            if sum(donejobs)==len(status):
                break
    
        pool.close()
        pool.join()
    else:
        donetotal=0
        for i in xrange(1,len(clustmat)):
            rslt = getjd_vect(i)
            n1 = clustnames[:-i]
            n2 = clustnames[i:]
            for l,r in enumerate(rslt):
                if r >= score:
                    tf.write(n1[l]+"\t"+n2[l]+"\t"+str(r)+"\n")
            donetotal+=len(rslt)
            print 'Wrote %s / %s records (%03.2f %%)' % (donetotal,totnum,100*float(donetotal)/totnum)
    if len(fout):
        os.rename(tf.name,fout)
# Commandline Execution
if __name__ == '__main__':
    mp.freeze_support()
    parser = argparse.ArgumentParser(description="""calcualte cluster similarity using jaccard and domain duplication index""")
    parser.add_argument("input",nargs="?", help="Cluster tab separated list example:   clusterId001   {'pfam00553': 3, 'pfam04978': 1, 'pfam00106': 3, 'pfam13560': 1, 'pfam00535': 1, 'pfam13421': 2, 'pfam00041': 1}  ",default=sys.stdin)
    parser.add_argument("-c", "--cpu", help="Number of cpus (default: Max, disable multiprocessing by setting to 1)", type=int, default=mp.cpu_count())
    parser.add_argument("-s", "--score", help="Similarity cutoff (default: 0.5)", type=float, default=0.5)
    parser.add_argument("-x", "--exclude", help="List of domains to exclude", default=[])
    parser.add_argument("-o", "--outfile", help="Output file (default: result_xxx)", default="")
    args = parser.parse_args()
    runlist(args.input,args.outfile,args.cpu,args.score,args.exclude)