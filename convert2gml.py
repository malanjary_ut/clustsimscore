#!/usr/bin/env python

"""
convert2gml.py: convert pairwise distance table to gml format and add note attributes from file
"""
__author__ = "Mohammad Alanjary"
__version__ = "1.0.1"

import networkx as nx
import argparse

def convert2gml(fname,outname,attr=None,d="\t"):
    G = nx.Graph()
    alledges=[]
    with open(fname,"U") as fil:
        for line in fil:
            x=line.strip().split(d)
            if len(x)>=3:
                alledges.append([str(x[0]),str(x[1]),float(x[2])])
    G.add_weighted_edges_from(alledges)
    nodedict = {}
    if attr:
        with open(attr,"r") as afil:
            atthead = afil.next().strip().split()[1:]
            for line in afil:
                x = line.strip().split()
                if x[0] in G.nodes():
                    nodedict[x[0]] = x[1:]
            for i,x in enumerate(atthead):
                print nodedict.keys()
                nx.set_node_attributes(G,x,{k:v[i] for k,v in nodedict.items()})
    nx.write_gml(G,outname)

# Commandline Execution
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="""Convert table to gml file""")
    parser.add_argument("input", help="Table to convert (3 columns, ex row: node1	node2	0.889 )")
    parser.add_argument("-d", "--delim", help="Delimiter. (default=Tab separated columns)", default="\t")
    parser.add_argument("-a", "--attr", help="Table of node attributes (tab separated, attribute titles taken from first line)", default=None)
    parser.add_argument("-o", "--outgml", help="Filename for gml output (default=output.gml)",default="output.gml")
    args = parser.parse_args()
    convert2gml(args.input, args.outgml, args.attr, args.delim)