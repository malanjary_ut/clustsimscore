
### clustsimscore.py

Method uses pfam composition similarity to calculate distance as described in:
Cimermancic, P., Medema, M. H., Claesen, J., Kurita, K., Wieland Brown, L. C., Mavrommatis, K., Fischbach, M. a. (2014).
Insights into secondary metabolism from a global analysis of prokaryotic biosynthetic gene clusters

takes list of pfam results in tsv format as input (see example.pflist)


#Usage (see -h for more options):

python clustsimscore2.py example.pflist -o pairwise_dists.txt


### convert2gml.py

Helper script to add attributes to nodes and export in gml format

#Usage (see -h for more options):

python convert2gml.py pairwise_dists.txt -a example_attr.txt -o output_graph.gml